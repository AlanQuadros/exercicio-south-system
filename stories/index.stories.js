import React from 'react';
import { storiesOf } from '@storybook/react';
import { CircularProgress } from '@material-ui/core';
import Button from 'components/Button';
import DragonDetailItem from 'components/DragonDetailItem';
import Input from 'components/Input';

// Button
storiesOf('Button', module)
  .add('example', () => ( 
    <Button 
      text={'Text'}
      onClick={() => alert('Clicked')}
    /> 
  ))
  .add('with loader', () => (
    <Button 
      text={<CircularProgress size={24} />} 
      onClick={() => alert('Clicked')}
    /> 
  ))
  .add('disabled with loader', () => (
    <Button 
      text={'Disabled'} 
      disabled
      onClick={() => alert('Clicked')}
    /> 
  ));

// DragonDetailItem
storiesOf('DragonDetailItem', module)
.addDecorator(storyFn => <div style={{ backgroundColor: 'black' }}>{storyFn()}</div>)
  .add('example', () => (
    <DragonDetailItem 
      label={'Label'}
      info={'Info'}
    />
  ))
  .add('only label', () => (
    <DragonDetailItem 
      label={'Label'}
    />
  ))
  .add('only info', () => (
    <DragonDetailItem 
      info={'Info'}
    />
  ))
  .add('with long texts', () => (
    <DragonDetailItem 
      label={'Label Label Label Label Label Label Label Label Label Label Label Label Label Label Label Label'}
      info={'Info Info Info Info Info Info Info Info Info Info Info Info Info Info Info Info Info Info Info Info'}
    />
  ));

// Input
storiesOf('Input', module)
  .add('example', () => (
    <Input 
      label={'Placeholder'}
    />
  ))
  .add('with value', () => (
    <Input 
      label={'Placeholder'}
      value={'Value'}
    />
  ))