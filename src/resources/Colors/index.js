export default {
  primary: '#212121',
  secondary: '#333333',
  green: '#005500',
  white: '#ffffff',
  black: '#000000',
}