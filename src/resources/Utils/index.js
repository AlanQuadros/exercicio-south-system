import React from "react";
import {
  Route,
  Redirect,
} from "react-router-dom";
import { format } from "date-fns";

function PrivateRoute({ logged, component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        logged ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
}

const tableColumns = [
  { title: 'Data de criação', field: 'createdAt', render: rowData => <p>{format(rowData.createdAt, 'DD/MM/YYYY')}</p>, editable: 'never' },
  { title: 'Nome', field: 'name', defaultSort: 'asc' },
  { title: 'Tipo', field: 'type' },
]

export {
  PrivateRoute,
  tableColumns,
}