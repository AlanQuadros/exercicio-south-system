export default {
  appName: 'Snogard',
  errorMessage: {
    loginError: 'Usuário ou senha incorretos.',
    updatedDragonError: 'Erro ao atualizar o dragão.',
    deletedDragonError: 'Erro ao deletar o dragão.',
    createDragonError: 'Erro ao cadastrar o dragão.',
    loadError: 'Erro ao carregar os dragões.',
  },
  networkMessages: {
    updatedDragon: 'Dragão atualizado com sucesso.',
    deletedDragon: 'Dragão deletado com sucesso.',
    createdDragon: 'Dragão cadastrado com sucesso.'
  },
  loginScreen: {
    user: 'Usuário',
    password: 'Senha',
    button: 'Entrar',
  },
  dragonDetailsScreen: {
    title: 'Detalhes do dragão',
    name: 'Nome',
    type: 'Tipo',
    creationDate: 'Data de criação',
  },
  dragonsScreen: {
    title: 'Dragões',
    logoff: 'Sair',
    createDragon: 'Criar Dragão',
    tableStrings: {
      pagination: {
        labelDisplayedRows: '{from}-{to} de {count}',
        labelRowsSelect: 'linhas',
        firstTooltip: 'Primeira página',
        previousTooltip: 'Página anterior',
        nextTooltip: 'Próxima página',
        lastTooltip: 'Última página',
      },
      header: {
        actions: 'Ações'
      },
      body: {
        addTooltip: 'Adicionar',
        emptyDataSourceMessage: 'Nenhum dragão cadastrado :(',
        editRow: {
          deleteText: 'Tem certeza que deseja deletar este dragão?',
          cancelTooltip: 'Cancelar',
          saveTooltip: 'Salvar'
        },
      },
      toolbar: {
        searchPlaceholder: 'Pesquisar',
        searchTooltip: 'Pesquisar',
      }
    },
  },
}