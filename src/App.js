import React from 'react';
import {connect} from "react-redux";
import { Route, Switch, withRouter } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';
import { SnackbarProvider } from 'notistack';
import { PrivateRoute } from 'resources/Utils';
import Colors from 'resources/Colors';
import LoginPage from 'pages/Login';
import DragonsListPage from 'pages/DragonsList';
import CreateDragonPage from 'pages/CreateDragon';
import DragonDetailsPage from 'pages/DragonDetails';
import NotFoundPage from 'pages/NotFound';

function App({userLogged}) {
  
  const theme = createMuiTheme({
    palette: {
      type: 'dark',
      primary: {
        main: Colors.white,
      },
    },
  });

  return (
    <MuiThemeProvider theme={theme}>
      <SnackbarProvider 
        maxSnack={3}
        autoHideDuration={3000}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}>
        <Switch>
          <Route exact path="/" component={() => <LoginPage />} />
          <PrivateRoute logged={userLogged} path="/list" component={() => <DragonsListPage />} />
          <PrivateRoute logged={userLogged} path="/create" component={() => <CreateDragonPage />} />
          <PrivateRoute logged={userLogged} path="/details" component={() => <DragonDetailsPage />} />
          <Route component={NotFoundPage} />
        </Switch>
      </SnackbarProvider>
    </MuiThemeProvider>
  );
}

const mapStateToProps = state => ({
  userLogged: state.UserReducer.userLogged,
});

export default withRouter(connect(mapStateToProps, null)(App));
