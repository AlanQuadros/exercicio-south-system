import React, { Component } from 'react';
import { BoxView } from './styles';

export default class BoxContainer extends Component {
  render() {
    return (
      <BoxView>
        { this.props.children }
      </BoxView>
    );
  }
}
