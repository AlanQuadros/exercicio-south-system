import styled from 'styled-components';
import { Box } from '@material-ui/core';
import Colors from 'resources/Colors';

export const BoxView = styled(Box)`
  background-color: ${Colors.secondary};
  margin: 32px;
  padding: 32px;
  border-radius: 3px;
`;
