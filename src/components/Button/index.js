import React from 'react';
import { ButtonContainer } from './styles';
import { CircularProgress } from '@material-ui/core';

export default function Button(props) {
  return (
    <ButtonContainer variant="contained" color="default" {...props}>
      { !props.disabled ? props.text : <CircularProgress size={24} /> }
    </ButtonContainer>
  );
}
