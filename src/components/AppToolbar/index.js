import React, { Component } from 'react';
import { AppToolbarContainer } from './styles';

export default class AppToolbar extends Component {
  render() {
    return (
      <AppToolbarContainer>
        { this.props.children }
      </AppToolbarContainer>
    );
  }
}
