import styled from 'styled-components';
import { Toolbar } from '@material-ui/core';
import Colors from 'resources/Colors';

const AppToolbarContainer = styled(Toolbar)`
  justify-content: space-between;
  background-color: ${Colors.secondary};
`;

export {
  AppToolbarContainer,
}