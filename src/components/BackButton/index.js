import React, { Component } from 'react';
import { Icon } from '@material-ui/core';
import { BrowserRouter } from 'react-router-dom';

export default class BackButton extends Component {
  render() {
    const history = new BrowserRouter().history;

    return (
      <Icon
        onClick={() => history.goBack()}
        color={'primary'}>
          arrow_back
      </Icon>
    );
  }
}
