import React from 'react';
import { Label, Info } from './styles';
import { Grid } from '@material-ui/core';

export default function DragonDetailItem(props) {
  return (
    <Grid item xs={12}>
      <Label>{ props.label }</Label>
      <Info>{ props.info }</Info>
    </Grid>
  );
}
