import styled from 'styled-components';
import Colors from 'resources/Colors';

const Label = styled.p`
  color: ${Colors.white};
  font-size: 19px;
  font-weight: bold;
`;

const Info = styled.p`
  color: ${Colors.white};
  font-size: 17px;
`;

export {
  Label,
  Info,
}