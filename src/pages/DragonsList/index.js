import React, { Component } from 'react';
import {connect} from "react-redux";
import { bindActionCreators } from 'redux';
import * as UserActions from 'actions/UserActions';
import * as DragonActions from 'actions/DragonActions';
import { Typography, AppBar, Grid } from '@material-ui/core';
import Constants from 'resources/Constants';
import { tableColumns } from 'resources/Utils';
import Button from 'components/Button';
import AppToolbar from 'components/AppToolbar';
import MaterialTable from 'material-table';
import { withRouter } from "react-router-dom";
import { withSnackbar } from 'notistack';

export class DragonsListPage extends Component {

  state = {
    data: [],
    loading: true,
  }
  
  handleLogoff(){
    this.props.doLogoff();
  }

  async loadDragons() {
    this.setState({ loading: true });
    try {
      await this.props.getDragons();
    } catch (e) {
      this.props.enqueueSnackbar(e.message, { variant: 'error' });
    }
    this.setState({ data: this.props.dragons, loading: false });
  }

  updateDragon = async (updatedDragon) => {
    try {
      const response = await this.props.updateDragon(updatedDragon);    
  
      this.props.enqueueSnackbar(
        response.message, 
        { variant: response.error ? 'error' : 'success' }
      );
      
      this.loadDragons();
    } catch (e) {
      this.props.enqueueSnackbar(e.message, { variant: 'error' });
    }
  }

  deleteDragon = async (deletedDragon) => {
    try {
      const response = await this.props.deleteDragon(deletedDragon);    
  
      this.props.enqueueSnackbar(
        response.message, 
        { variant: response.error ? 'error' : 'success' }
      );
      
      this.loadDragons();
    } catch (e) {
      this.props.enqueueSnackbar(e.message, { variant: 'error' });
    }
  }

  createNewDragon = () => {
    this.props.history.push('/create');
  }

  goToDetails = (dragon) => {
    this.props.history.push({
      pathname: '/details',
      state: { dragon },
    });
  }

  componentDidMount() {
    this.loadDragons();
  }

  render() {
    return (
      <Grid container justify="center">
        <AppBar position="static">
          <AppToolbar>
            <Button 
              text={Constants.dragonsScreen.createDragon} 
              onClick={() => this.createNewDragon()} 
              color={'default'} />

            <Typography color={'primary'} variant="h6">
              { Constants.appName }
            </Typography>

            <Button
              text={Constants.dragonsScreen.logoff} 
              onClick={this.handleLogoff.bind(this)} 
              color={'default'} />

          </AppToolbar>
        </AppBar>
        
        <MaterialTable
          isLoading={this.state.loading}
          style={{ width: '90%', marginTop: 20, marginBottom: 20 }}
          title={Constants.dragonsScreen.title}
          columns={tableColumns}
          data={this.state.data}
          localization={Constants.dragonsScreen.tableStrings}
          options={{
            actionsColumnIndex: -1,
            sorting: false,
          }}
          editable={{
            onRowUpdate: (newData, oldData) => this.updateDragon(newData),
            onRowDelete: oldData => this.deleteDragon(oldData),
          }}
          onRowClick={(event, rowData) => this.goToDetails(rowData)}
        />
      </Grid>
    );
  }
}

const mapStateToProps = state => ({
  userLogged: state.UserReducer.userLogged,
  dragons: state.DragonReducer.dragons,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    ...UserActions,
    ...DragonActions,
    },
    dispatch,
  );
}

export default withSnackbar(withRouter(connect(mapStateToProps, mapDispatchToProps)(DragonsListPage)));