import React, { Component } from 'react';
import {connect} from "react-redux";
import * as DragonActions from 'actions/DragonActions';
import { ButtonContainer } from './styles';
import { Typography, AppBar, Grid, Container } from '@material-ui/core';
import Constants from 'resources/Constants';
import Input from 'components/Input';
import Button from 'components/Button';
import AppToolbar from 'components/AppToolbar';
import BoxContainer from 'components/BoxContainer';
import BackButton from 'components/BackButton';
import { withRouter } from "react-router-dom";
import { withSnackbar } from 'notistack';

export class CreateDragonPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      type: '',
      loading: false,
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  createNewDragon = async () => {
    const { name, type } = this.state;

    if (name.length > 0 && type.length > 0) {
      this.setState({ loading: true });
      try {
        const response = await this.props.createDragon({ name, type });

        this.props.enqueueSnackbar(
          response.message, 
          { variant: response.error ? 'error' : 'success' }
        );

        this.props.history.push('/list');
      } catch (e) {
        this.setState({ loading: false });
        this.props.enqueueSnackbar(e.message, { variant: 'error' });
      }
    }
  }

  render() {
    return (
      <Grid container justify="center">  
        <AppBar position="static">
          <AppToolbar>
            <BackButton />
            
            <Typography color={'primary'} variant="h6">
              { Constants.dragonsScreen.createDragon }
            </Typography>

            <div />
          </AppToolbar>
        </AppBar>

        <Container component="main" maxWidth="xs">
          <BoxContainer>
            <Grid container spacing={2} justify="center">
              <Grid item xs={12}>
                <Input fullWidth label={'Nome'} name="name" required onChange={this.handleChange} />
              </Grid>

              <Grid item xs={12}>
                <Input fullWidth label={'Tipo'} name="type" required onChange={this.handleChange} />
              </Grid>

              <Grid item xs={12}>
                <ButtonContainer onClick={() => this.createNewDragon()}>
                  <Button fullWidth text={'Cadastrar'} disabled={this.state.loading} />
                </ButtonContainer>
              </Grid>
            </Grid>
          </BoxContainer>
        </Container>
      </Grid>
    );
  }
}

export default withSnackbar(withRouter(connect(null, DragonActions)(CreateDragonPage)));