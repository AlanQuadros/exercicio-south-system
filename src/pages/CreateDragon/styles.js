import styled from 'styled-components';

const ButtonContainer = styled.div`
  margin-top: 10px;
`;

export {
  ButtonContainer,
}
