import React from 'react';
import { Container } from './styles';
import { Typography } from '@material-ui/core';

export default function NotFoundPage() {
  return (
    <Container>
      <Typography color={'primary'} variant="h6">
        { 'Página não encontrada' }
      </Typography>
    </Container>
  );
}
