import React, { Component } from 'react';
import { Typography, AppBar, Grid, Container } from '@material-ui/core';
import Constants from 'resources/Constants';
import AppToolbar from 'components/AppToolbar';
import BoxContainer from 'components/BoxContainer';
import BackButton from 'components/BackButton';
import DragonDetailItem from 'components/DragonDetailItem';
import { withRouter } from "react-router-dom";
import { format } from 'date-fns';

export class DragonDetailsPage extends Component {
  render() {
    if (this.props.location.state === undefined) {
      this.props.history.push('/');
      return null;
    }

    const { dragon } = this.props.location.state;

    return (
      <Grid container justify="center">
        <AppBar position="static">
          <AppToolbar>
            <BackButton />

            <Typography color={'primary'} variant="h6">
              { Constants.dragonDetailsScreen.title }
            </Typography>
            <div />
          </AppToolbar>
        </AppBar>

        <Container component="main" maxWidth="xs">
          <BoxContainer>
            <Grid container spacing={2} justify="center">
              <DragonDetailItem 
                label={Constants.dragonDetailsScreen.creationDate}
                info={format(dragon.createdAt, 'DD/MM/YYYY')}
              />

              <DragonDetailItem 
                label={Constants.dragonDetailsScreen.name}
                info={dragon.name}
              />

              <DragonDetailItem 
                label={Constants.dragonDetailsScreen.type}
                info={dragon.type}
              />
            </Grid>
          </BoxContainer>
        </Container>
      </Grid>
    );
  }
}

export default withRouter(DragonDetailsPage);