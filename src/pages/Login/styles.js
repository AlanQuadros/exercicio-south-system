import styled from 'styled-components';
import { 
  Box,
  Grid,
} from '@material-ui/core';
import Colors from 'resources/Colors';

const ContainerView = styled(Grid)`
  align-items: center;
  height: 100vh;
`;

const LoginContainer = styled(Box)`
  background-color: ${Colors.secondary};
  border-radius: 5px;
  padding: 32px;
  width: 300px;
  display: flex;
  flex-direction: column;
`;

const InputContainer = styled.div`
  margin: 10px 0;
  display: flex;
  flex-direction: column;
`;

const ButtonContainer = styled.div`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
`;

const LoginTitle = styled.p`
  color: ${Colors.white};
  text-align: center;
  font-weight: bold;
  font-size: 22px;
`;

export {
  ContainerView,
  LoginContainer,
  InputContainer,
  ButtonContainer,
  LoginTitle,
}
