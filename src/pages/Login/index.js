import React, { Component } from 'react';
import {connect} from "react-redux";
import * as UserActions from 'actions/UserActions';
import {
  LoginContainer,
  ContainerView,
  InputContainer,
  ButtonContainer,
  LoginTitle,
} from './styles';
import Constants from 'resources/Constants';
import Input from 'components/Input';
import Button from 'components/Button';

import { withRouter } from "react-router-dom";
import { withSnackbar } from 'notistack';

export class LoginPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
    }

    this.handleChange = this.handleChange.bind(this);
  }

  onClickLogin = () => {
    const { login, password } = this.state;

    const loginResponse = this.props.doLogin({ login, password });
    
    if (!loginResponse.error) {
      this.props.history.push('/list')
    } else {
      this.props.enqueueSnackbar(loginResponse.message, { variant: 'error' });
    }
  }

  onKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.onClickLogin();
    }
  }

  handleChange(evt) {
    this.setState({ [evt.target.name]: evt.target.value });
  }

  componentDidMount() {
    if (this.props.userLogged) {
      this.props.history.push('/list');
    }
  }

  render() {
    const { login, password } = this.state;

    return (
      <ContainerView container justify="center">
        <LoginContainer>
          <LoginTitle>{ Constants.appName }</LoginTitle>

          <InputContainer>
            <Input
              label={Constants.loginScreen.user}
              value={login}
              name={'login'}
              onChange={this.handleChange}
              onKeyPress={(event) => this.onKeyPress(event)}
            />
          </InputContainer>

          <InputContainer>
            <Input
              label={Constants.loginScreen.password}
              value={password}
              type={'password'}
              name={'password'}
              onChange={this.handleChange}
              onKeyPress={(event) => this.onKeyPress(event)}
            />
          </InputContainer>

          <ButtonContainer onClick={() => this.onClickLogin()}>
            <Button text={Constants.loginScreen.button} />
          </ButtonContainer>
        </LoginContainer>

      </ContainerView>
    );
  }
}

const mapStateToProps = state => ({
  userLogged: state.UserReducer.userLogged,
});

export default withSnackbar(withRouter(connect(mapStateToProps, UserActions)(LoginPage)));