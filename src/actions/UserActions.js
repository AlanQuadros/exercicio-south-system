import { USER_LOGGED } from 'reducers/types';
import Constants from 'resources/Constants';

export const doLogin = (user) => (dispatch) => {
  if (user.login === 'admin' && user.password === 'admin') {
    dispatch({
      type: USER_LOGGED,
      userLogged: true,
    });

    return { error: false };
  } else {
    dispatch({
      type: USER_LOGGED,
      userLogged: false,
    });

    return {
      error: true,
      message: Constants.errorMessage.loginError,
    }
  }
}

export const doLogoff = () => (dispatch) => {
  dispatch({
    type: USER_LOGGED,
    userLogged: false,
  });
}