import axiosInstance, { DRAGON } from "services";
import { LOAD_DRAGONS } from "reducers/types";
import Constants from "resources/Constants";

export const getDragons = () => async (dispatch) => {
  try {
    const response = await axiosInstance(DRAGON);

    dispatch({
      type: LOAD_DRAGONS,
      dragons: response.data,
    });
  } catch (e) {
    throw new Error(Constants.errorMessage.loadError);
  }
}

export const createDragon = (dragon) => async (dispatch) => {
  try {
    const response = await axiosInstance({
      method: 'POST',
      url: DRAGON,
      data: dragon,
    });

    if (response.status >= 200 && response.status < 400) {
      return {
        error: false,
        message: Constants.networkMessages.createdDragon,
      };
    } else {
      return {
        error: true,
        message: Constants.errorMessage.createDragonError,
      };
    }
    
  } catch (e) {
    console.error('Error createDragon', e);
    throw new Error(Constants.errorMessage.createDragonError);
  }
}

export const updateDragon = (dragon) => async (dispatch) => {
  try {
    const response = await axiosInstance({
      method: 'PUT',
      url: DRAGON + dragon.id,
      data: dragon,
    });

    if (response.status >= 200 && response.status < 400) {
      return {
        error: false,
        message: Constants.networkMessages.updatedDragon,
      };
    } else {
      return {
        error: true,
        message: Constants.errorMessage.updatedDragonError,
      };
    }
    
  } catch (e) {
    console.error('Error updateDragon', e);
    throw new Error(Constants.errorMessage.updatedDragonError);
  }
}

export const deleteDragon = (dragon) => async (dispatch) => {
  try {
    const response = await axiosInstance({
      method: 'DELETE',
      url: DRAGON + dragon.id,
    });

    if (response.status >= 200 && response.status < 400) {
      return {
        error: false,
        message: Constants.networkMessages.deletedDragon,
      };
    } else {
      return {
        error: true,
        message: Constants.errorMessage.deletedDragonError,
      };
    }
    
  } catch (e) {
    console.error('Error deleteDragon', e);
    throw new Error(Constants.errorMessage.deletedDragonError);
  }
}