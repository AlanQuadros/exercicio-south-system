import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'https://5c4b2a47aa8ee500142b4887.mockapi.io/api/v1/',
  timeout: 30000,
});

export const DRAGON = '/dragon/';

export default axiosInstance;