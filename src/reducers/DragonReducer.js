import { combineReducers } from 'redux';
import { LOAD_DRAGONS } from './types';

const dragons = (state = [], action) => {
  switch (action.type) {
    case LOAD_DRAGONS:
      return action.dragons;
    default:
      return state;
  }
};

export default combineReducers({
  dragons,
});
