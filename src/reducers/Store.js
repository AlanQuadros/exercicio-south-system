import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk";
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import UserReducer from './UserReducer';
import DragonReducer from './DragonReducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: [
    'UserReducer',
  ],
}

const rootReducer = combineReducers({
  UserReducer,
  DragonReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);