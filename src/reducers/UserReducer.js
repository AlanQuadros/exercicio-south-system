import { combineReducers } from 'redux';
import { USER_LOGGED } from './types';

const userLogged = (state = false, action) => {
  switch (action.type) {
    case USER_LOGGED:
      return action.userLogged;
    default:
      return state;
  }
};

export default combineReducers({
  userLogged,
});
