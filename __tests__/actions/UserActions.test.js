import configureStore from 'redux-mock-store';
import thunk from "redux-thunk";
import * as UserActions from '../../src/actions/UserActions';

const mockStore = configureStore([thunk]);
const store = mockStore();

describe('UserActions', () => {
  beforeEach(() => { // Runs before each test in the suite
    store.clearActions();
  });

  describe('doLogin', () => {
    test('Do login and return userLogged true', () => {
      const user = {
        login: 'admin',
        password: 'admin',
      }

      const expectedActions = [
        {
          'type': 'USER_LOGGED',
          'userLogged': true,
        },
      ];
  
      store.dispatch(UserActions.doLogin(user));
      expect(store.getActions()).toEqual(expectedActions);
    });

    test('Do login and return userLogged false', () => {
      const user = {
        login: 'admin',
        password: '',
      }

      const expectedActions = [
        {
          'type': 'USER_LOGGED',
          'userLogged': false,
        },
      ];
  
      store.dispatch(UserActions.doLogin(user));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('doLogoff', () => {
    test('Do logoff and return userLogged false', () => {
      const expectedActions = [
        {
          'type': 'USER_LOGGED',
          'userLogged': false,
        },
      ];
  
      store.dispatch(UserActions.doLogoff());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

});