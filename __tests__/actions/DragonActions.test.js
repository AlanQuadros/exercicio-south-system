import configureStore from 'redux-mock-store';
import thunk from "redux-thunk";
import * as DragonActions from '../../src/actions/DragonActions';

const mockStore = configureStore([thunk]);
const store = mockStore();

describe('DragonActions', () => {
  beforeEach(() => {
    store.clearActions();
  });

  describe('getDragons', () => {
    test('Fetch all dragons', () => {
      store.dispatch(DragonActions.getDragons());
      expect(store.getActions()).toEqual([]);
    });
  });

  describe('createDragon', () => {
    test('Create new dragon', () => {
      const dragon = {
        name: 'Drogo',
        type: 'Fire',
      }

      store.dispatch(DragonActions.createDragon(dragon));
      expect(store.getActions()).toEqual([]);
    });
  });

  describe('updateDragon', () => {
    test('Update a dragon', () => {
      const dragon = {
        id: 1,
        name: 'Drogo',
        type: 'Fire',
      }

      store.dispatch(DragonActions.updateDragon(dragon));
      expect(store.getActions()).toEqual([]);
    });
  });

  describe('deleteDragon', () => {
    test('Delete a dragon', () => {
      const dragon = {
        id: 1,
        name: 'Drogo',
        type: 'Fire',
      }

      store.dispatch(DragonActions.deleteDragon(dragon));
      expect(store.getActions()).toEqual([]);
    });
  });

});