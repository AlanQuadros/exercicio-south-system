import React from 'react'
import NotFoundPage from '../../src/pages/NotFound';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('pages', () => {
  describe('not found page', () => {
    it('should render a not found page', () => {
      const component = shallow(<NotFoundPage />)
      expect(component).toMatchSnapshot();
    })
  })
})