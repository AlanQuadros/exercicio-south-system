import React from 'react'
import { LoginPage } from '../../src/pages/Login';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('pages', () => {
  describe('login page', () => {
    it('should render a login page', () => {
      const component = shallow(<LoginPage />)
      expect(component).toMatchSnapshot();
    })
  })
})