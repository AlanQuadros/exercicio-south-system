import React from 'react'
import { CreateDragonPage } from '../../src/pages/CreateDragon';
import { shallow, mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Input from 'components/Input';

configure({ adapter: new Adapter() })

describe('pages', () => {
  describe('create dragon page', () => {
    it('should render a create dragon page', () => {
      const component = shallow(<CreateDragonPage />)
      expect(component).toMatchSnapshot();
    })

    it('should start a new create dragon form with empty state', () => {
      const component = mount(<CreateDragonPage />)
      expect(component).toEqual({})
      expect(component.state().name).toEqual('')
      expect(component.state().type).toEqual('')
      component.unmount()
    })

    it('should start a new create dragon form and set state', () => {
      const component = mount(<CreateDragonPage />)
      component.setState({ name: 'foo', type: 'bar' })
      expect(component).toEqual({})
      expect(component.state().name).toEqual('foo')
      expect(component.state().type).toEqual('bar')
      component.unmount()
    })

    it('should change name', () => {
      const event = { target: { name: "name", value: "Nome" } };
      const component = shallow(<CreateDragonPage />);
      component.instance().handleChange(event)
      expect(component.state().name).toEqual('Nome')
    })

    it('should change type', () => {
      const event = { target: { name: "type", value: "Tipo" } };
      const component = shallow(<CreateDragonPage />);
      component.instance().handleChange(event)
      expect(component.state().type).toEqual('Tipo')
    })

    it('should click in create new dragon', () => {
      const component = shallow(<CreateDragonPage />);
      component.setState({ name: 'foo', type: 'bar' })
      component.instance().createNewDragon()
      expect(component.state().name.length).toBeGreaterThan(2)
      expect(component.state().type.length).toBeGreaterThan(2)
    })

  })
})