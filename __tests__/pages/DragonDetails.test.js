import React from 'react'
import { DragonDetailsPage } from '../../src/pages/DragonDetails';
import { shallow, mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('pages', () => {
  describe('dragon details page', () => {
    it('should render a dragon details page', () => {
      const component = shallow(<DragonDetailsPage location={{ state: undefined }} history={{ push: jest.fn() }} />)
      expect(component).toMatchSnapshot();
    })

    it('should start a dragon details page with props', () => {
      const dragon = {
        name: 'Drogon',
        type: 'Blue',
      }

      const component = mount(<DragonDetailsPage location={{ state: { dragon } }} />)
      expect(component).toEqual({})
      expect(component.props().location.state.dragon.name).toEqual('Drogon')
      expect(component.props().location.state.dragon.type).toEqual('Blue')
      component.unmount()
    })

    it('should start a dragon details page without props', () => {
      const component = mount(<DragonDetailsPage location={{ state: undefined }} history={{ push: jest.fn() }} />)
      expect(component).toEqual({})
      expect(component.props().location.state).toBeUndefined()
      component.unmount()
    })

  })
})