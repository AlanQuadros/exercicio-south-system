import React from 'react'
import { DragonsListPage } from '../../src/pages/DragonsList';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('pages', () => {
  describe('dragon list page', () => {
    it('should render a dragon list page', () => {
      const component = shallow(<DragonsListPage />)
      expect(component).toMatchSnapshot();
    })

    it('should render a dragon list page and set state data', () => {
      const component = mount(<DragonsListPage />)
      component.setState({ data: [{ name: '' }] })
      expect(component).toEqual({})
      expect(component.state().data.length).toEqual(1)
      component.unmount()
    })

    it('should click in loadDragons', () => {
      const component = shallow(<DragonsListPage />);
      jest.spyOn(DragonsListPage.prototype, 'loadDragons')
        .mockImplementation(() => component.setState({ loading: false }))
      component.instance().loadDragons()
      expect(component.state().loading).toBeFalsy()
    })

    it('should click in logoff', () => {
      let spy = jest.spyOn(DragonsListPage.prototype, 'handleLogoff')
      .mockImplementation(() => 1)
      const component = shallow(<DragonsListPage />);
      component.instance().handleLogoff()
      expect(spy).toHaveBeenCalled();
    })
  })
})