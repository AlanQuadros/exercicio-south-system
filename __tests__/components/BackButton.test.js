import React from 'react'
import BackButton from 'components/BackButton';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('components', () => {
  describe('BackButton', () => {
    it('should render a BackButton', () => {
      const component = shallow(<BackButton />)
      expect(component).toMatchSnapshot();
    })

    it('should call onClick prop', () => {
      const wrapper = shallow(<BackButton />)
      wrapper.instance().onClick = jest.fn()
      wrapper.update()
      wrapper.instance().onClick('click')
      expect(wrapper.instance().onClick).toBeCalledWith('click')
    })
  })
})