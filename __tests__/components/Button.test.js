import React from 'react'
import Button from 'components/Button';
import { mount, configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('components', () => {
  describe('button', () => {
    it('should render a button', () => {
      const component = shallow(<Button text={'Button'} />)
      expect(component).toMatchSnapshot();
    })

    it('should render a button with prop text', () => {
      const component = mount(<Button text={'Button'} />)
      expect(component).toEqual({})
      expect(component.props().text).toEqual('Button')
      component.unmount()
    })

    it('should render a button without props', () => {
      const component = mount(<Button />)
      expect(component).toEqual({})
      expect(component.props().text).toBeUndefined()
      component.unmount()
    })

  })
})