import React from 'react'
import AppToolbar from 'components/AppToolbar';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('components', () => {
  describe('AppToolbar', () => {
    it('should render a AppToolbar', () => {
      const component = shallow(<AppToolbar />)
      expect(component).toMatchSnapshot();
    })
  })
})