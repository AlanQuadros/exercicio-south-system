import React from 'react'
import Input from 'components/Input';
import { mount, configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { TextField } from '@material-ui/core';

configure({ adapter: new Adapter() })

describe('components', () => {
  describe('input', () => {
    it('should render a input', () => {
      const component = shallow(<Input label={'Input'} />)
      expect(component).toMatchSnapshot();
    })

    it('should render a input with prop label', () => {
      const component = mount(<Input label={'Input'} />)
      expect(component).toEqual({})
      expect(component.props().label).toEqual('Input')
      component.unmount()
    })

    it('should render a input with prop value', () => {
      const component = mount(<Input value={'Input'} />)
      expect(component).toEqual({})
      expect(component.props().value).toEqual('Input')
      component.unmount()
    })

    it('should render a input with props label and value', () => {
      const component = mount(<Input label={'Label'} value={'Value'} />)
      expect(component).toEqual({})
      expect(component.props().label).toEqual('Label')
      expect(component.props().value).toEqual('Value')
      component.unmount()
    })

    it('should render a input without props', () => {
      const component = mount(<Input />)
      expect(component).toEqual({})
      expect(component.props().label).toBeUndefined()
      expect(component.props().value).toBeUndefined()
      component.unmount()
    })

    it('should call onChange prop', () => {
      const onChangeMock = jest.fn();
      const component = shallow(<Input onChange={onChangeMock} />);
      component.find(TextField).simulate('change', 'Value');
      expect(onChangeMock).toBeCalledWith('Value');
    });

    it('should call onChange 1 time', () => {
      const onChangeMock = jest.fn();
      const component = shallow(<Input onChange={onChangeMock} />);
      component.find(TextField).simulate('change', 'Value');
      expect(onChangeMock).toHaveBeenCalled();
    });

  })
})