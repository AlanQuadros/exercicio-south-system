import React from 'react'
import DragonDetailItem from 'components/DragonDetailItem';
import { mount, configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('components', () => {
  describe('DragonDetailItem', () => {
    it('should render a DragonDetailItem', () => {
      const component = shallow(<DragonDetailItem label={'label'} info={'info'} />)
      expect(component).toMatchSnapshot();
    })

    it('should render a DragonDetailItem with props', () => {
      const component = mount(<DragonDetailItem label={'label'} info={'info'} />)
      expect(component).toEqual({})
      expect(component.props().label).toEqual('label')
      expect(component.props().info).toEqual('info')
      component.unmount()
    })

    it('should render a DragonDetailItem without props', () => {
      const component = mount(<DragonDetailItem />)
      expect(component).toEqual({})
      expect(component.props().label).toBeUndefined()
      expect(component.props().info).toBeUndefined()
      component.unmount()
    })

  })
})