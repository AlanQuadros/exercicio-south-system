import React from 'react'
import BoxContainer from 'components/BoxContainer';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() })

describe('components', () => {
  describe('BoxContainer', () => {
    it('should render a BoxContainer', () => {
      const component = shallow(<BoxContainer />)
      expect(component).toMatchSnapshot();
    })
  })
})