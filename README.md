# Snogard

Clique [**aqui**](https://exercicio-south-system.alanquadros.now.sh/) para abrir a aplicação.
Usuário: **admin**
Senha: **admin**

### Scripts
Na pasta raíz do projeto, você pode rodar:

#### `yarn`
Instala as dependências da aplicação.

#### `yarn start`
Roda a aplicação em modo de desenvolvimento.
Abra [http://localhost:3000](http://localhost:3000) para visualizar no navegador.

#### `yarn test`
Roda todos os testes do projeto.

#### `yarn storybook`
Roda o Storybook de componentes do projeto.
Abra [http://localhost:9001](http://localhost:9001) para visualizar no navegador.

#### `yarn run build`
Cria a aplicação para produção dentro da pasta `build`.

### Desenvolvimento

##### Principais bibliotecas utilizadas
 - Material UI
 - styled-components
 - Redux
 - Axios
 - date-fns

##### Bibliotecas de teste
 - Jest
 - Enzyme
 - Storybook

### Organização do projeto
##### Pastas
 - `__test__`
 - .storybook
 - coverage
 - public
 - src
	 - actions
	 - components
	 - pages
	 - reducers
	 - resources
	 - services
 - stories
